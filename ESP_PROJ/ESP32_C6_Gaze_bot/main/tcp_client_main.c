/*
 * SPDX-FileCopyrightText: 2022 Espressif Systems (Shanghai) CO LTD
 *
 * SPDX-License-Identifier: Unlicense OR CC0-1.0
 */
#include "nvs_flash.h"
#include "esp_netif.h"
#include "protocol_examples_common.h"
#include "esp_event.h"
#include "Motor_lib.h"
#include <driver/gpio.h>
#include "Servo_lib.h"
#include <esp_log.h>
#include "cJSON.h"



extern void tcp_client(void);

void app_main(void)
{
    ESP_ERROR_CHECK(nvs_flash_init());
    ESP_ERROR_CHECK(esp_netif_init());
    ESP_ERROR_CHECK(esp_event_loop_create_default());

    /* This helper function configures Wi-Fi or Ethernet, as selected in menuconfig.
     * Read "Establishing Wi-Fi or Ethernet Connection" section in
     * examples/protocols/README.md for more information about this function.
     */
    motor_init();
    vTaskDelay(1/portTICK_PERIOD_MS);

    servo_type_t Myservo = {0, 0, NULL};
    init_servo(&Myservo);
    vTaskDelay(1000/portTICK_PERIOD_MS);
    move_servo(&Myservo, 60);
    //motor_sit_on_hind_legs();
    //vTaskDelay(3000/portTICK_PERIOD_MS);
   // motor_stand_up_straight();
     ESP_ERROR_CHECK(example_connect());

     tcp_client();
    // while (1) {
    //     vTaskDelay(1000/portTICK_PERIOD_MS);
    // }

    //  motor_sit_on_hind_legs();
    // vTaskDelay(1000/portTICK_PERIOD_MS);
    // motor_stand_up_straight();

    // while(1) {
    //     printf("State: %d %d \r\n", gpio_get_level(GPIO_NUM_6), gpio_get_level(GPIO_NUM_7));
    //     vTaskDelay(100/portTICK_PERIOD_MS);
    // }
     //motor_stand_up_straight();


}
