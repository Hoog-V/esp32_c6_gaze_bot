#ifndef SERVO_LIB_H
#define SERVO_LIB_H

#include <stdint.h>
#include <driver/mcpwm_prelude.h>

typedef struct {
    uint8_t gpio_pin_num;
    int16_t current_servo_pos;
    mcpwm_cmpr_handle_t comparator;
} servo_type_t;

void init_servo(servo_type_t* servo);

void move_servo(servo_type_t* servo, int16_t degrees);

void deinit_servo();


#endif