#include "Motor_lib.h"
#include <driver/gpio.h>
#include "esp_event.h"

void motor_init() {
    gpio_set_direction(GPIO_NUM_1, GPIO_MODE_OUTPUT);
    gpio_set_direction(GPIO_NUM_10, GPIO_MODE_OUTPUT);
    gpio_set_direction(GPIO_NUM_6, GPIO_MODE_INPUT);
    gpio_set_direction(GPIO_NUM_7, GPIO_MODE_INPUT);
    gpio_set_pull_mode(GPIO_NUM_6, GPIO_PULLUP_ONLY);
    gpio_set_pull_mode(GPIO_NUM_7, GPIO_PULLUP_ONLY);
}

void motor_sit_on_hind_legs() {
    printf("Moving motor\r\n");
    gpio_set_level(GPIO_NUM_1, 1);
    gpio_set_level(GPIO_NUM_10, 0);
    uint64_t i =0;
    if(gpio_get_level(GPIO_NUM_6)) {
    while(gpio_get_level(GPIO_NUM_6)) {
        if(i++ > 10000000) {
            printf("TimeOut reached!\r\n");
            break;
        }
    }
    }
    printf("State: %d %d \r\n", gpio_get_level(GPIO_NUM_6), gpio_get_level(GPIO_NUM_7));
    gpio_set_level(GPIO_NUM_1, 0);
    gpio_set_level(GPIO_NUM_10, 0);
    vTaskDelay(100/portTICK_PERIOD_MS);
    gpio_set_level(GPIO_NUM_1, 0);
    gpio_set_level(GPIO_NUM_10, 1);
    vTaskDelay(100/portTICK_PERIOD_MS);
    printf("State: %d %d \r\n", gpio_get_level(GPIO_NUM_6), gpio_get_level(GPIO_NUM_7));
    gpio_set_level(GPIO_NUM_1, 0);
    gpio_set_level(GPIO_NUM_10, 0);
}

void motor_stand_up_straight() {
    printf("Moving motor\r\n");
    gpio_set_level(GPIO_NUM_1, 0);
    gpio_set_level(GPIO_NUM_10, 1);
    uint64_t i =0;
    if(gpio_get_level(GPIO_NUM_7)) {
    while(gpio_get_level(GPIO_NUM_7)) {
        if(i++ > 10000000) {
            printf("TimeOut reached!\r\n");
            break;
        }
    }
    }
    printf("State: %d %d \r\n", gpio_get_level(GPIO_NUM_6), gpio_get_level(GPIO_NUM_7));
    gpio_set_level(GPIO_NUM_1, 0);
    gpio_set_level(GPIO_NUM_10, 0);
    
}

void motor_wiggle_between_sit_and_straight_pos(uint8_t wiggle_times) {

}

void motor_deinit() {

}