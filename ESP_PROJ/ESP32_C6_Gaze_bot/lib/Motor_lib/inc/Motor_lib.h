#ifndef MOTOR_LIB_H
#define MOTOR_LIB_H
#include <stdint.h>

void motor_init();

void motor_sit_on_hind_legs();

void motor_stand_up_straight();

void motor_wiggle_between_sit_and_straight_pos(uint8_t wiggle_times);

void motor_deinit();


#endif // MOTOR_LIB_H